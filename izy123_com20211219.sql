-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2021-12-19 17:01:15
-- 服务器版本： 8.0.20
-- PHP 版本： 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `izy123_com`
--

-- --------------------------------------------------------

--
-- 表的结构 `izy_cate_info`
--

CREATE TABLE `izy_cate_info` (
  `cate_id` int UNSIGNED NOT NULL,
  `pid` int UNSIGNED DEFAULT '0',
  `cate_name` varchar(20) NOT NULL DEFAULT '' COMMENT '分类名称',
  `cate_color` char(6) DEFAULT '000000' COMMENT '文字颜色',
  `seo_title` varchar(50) DEFAULT NULL COMMENT 'seo标题',
  `seo_keyword` varchar(50) DEFAULT NULL COMMENT '关键字',
  `seo_desc` varchar(100) DEFAULT NULL COMMENT '描述',
  `data_num` int UNSIGNED DEFAULT '0' COMMENT '链接数',
  `click_num` int UNSIGNED DEFAULT '0' COMMENT '总点击数',
  `sort_order` int UNSIGNED DEFAULT '0' COMMENT '排序',
  `status` enum('delete','online','offline') DEFAULT 'online' COMMENT '状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `izy_cate_info`
--

INSERT INTO `izy_cate_info` (`cate_id`, `pid`, `cate_name`, `cate_color`, `seo_title`, `seo_keyword`, `seo_desc`, `data_num`, `click_num`, `sort_order`, `status`, `create_time`) VALUES
(10, 0, '学习 · 生活', '', '爱学习 · 爱生活', '学习,生活', '学习 · 生活', 0, 0, 1, 'online', '2021-12-18 14:30:34'),
(11, 0, '常用 · 社区', '', '常用 · 社区', '常用,社区', '常用 · 社区', 0, 0, 2, 'online', '2021-12-18 15:45:15'),
(12, 0, '影音 · 娱乐', '', '影音 · 娱乐', '影音,娱乐', '影音 · 娱乐', 0, 0, 3, 'online', '2021-12-18 15:55:00'),
(13, 0, '发现 · 世界', '', '发现 · 世界', '发现,世界', '发现 · 世界', 0, 0, 4, 'online', '2021-12-18 16:00:07'),
(14, 0, '在线 · 工具', '', '在线 · 工具', '在线,工具', '在线 · 工具', 0, 0, 6, 'online', '2021-12-18 16:05:53'),
(15, 0, '搜索 · 资源', '', '搜索 · 资源', '搜索,资源', '搜索 · 资源', 0, 0, 9, 'online', '2021-12-19 05:13:09'),
(16, 0, '隔壁 · 友链', '', '隔壁 · 友链', '隔壁,友链', '隔壁 · 友链', 0, 0, 99, 'online', '2021-12-19 06:34:14');

-- --------------------------------------------------------

--
-- 表的结构 `izy_link_info`
--

CREATE TABLE `izy_link_info` (
  `link_id` int UNSIGNED NOT NULL,
  `cate_id` int UNSIGNED NOT NULL DEFAULT '0' COMMENT '分类',
  `title` varchar(10) NOT NULL DEFAULT '网站标题' COMMENT '网站标题',
  `title_color` char(6) DEFAULT '000000' COMMENT '标题颜色#000000',
  `link` varchar(500) NOT NULL DEFAULT '' COMMENT '链接地址',
  `intro` varchar(50) DEFAULT NULL COMMENT '简介',
  `sort_order` int DEFAULT '0' COMMENT '排序值越大越靠前',
  `status` enum('delete','online','offline') DEFAULT 'offline' COMMENT '状态',
  `click_num` int UNSIGNED DEFAULT '0' COMMENT '总点击数',
  `last_time` timestamp NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `izy_link_info`
--

INSERT INTO `izy_link_info` (`link_id`, `cate_id`, `title`, `title_color`, `link`, `intro`, `sort_order`, `status`, `click_num`, `last_time`, `create_time`) VALUES
(63, 10, '惠购物', '', 'http://pca8m9qh-511819385.wikeyun.cn', '云真惠生活', 9, 'online', 0, '2021-12-19 05:18:36', '2021-12-18 14:31:26'),
(64, 10, '95折充值', '', 'https://www.xiaochou.ren/yunzhenlife', '电费话费限时95折充值！', 1, 'online', 0, '2021-12-19 05:17:40', '2021-12-18 15:23:14'),
(65, 10, '翻译', '', 'https://fanyi.youdao.com/', '在线有道翻译', 0, 'online', 0, '2021-12-18 15:24:24', '2021-12-18 15:24:19'),
(66, 10, '中国大学', '', 'https://www.icourse163.org', '中国大学Mooc慕课', 0, 'online', 0, '2021-12-18 15:38:56', '2021-12-18 15:38:52'),
(67, 10, '菜鸟教程', '', 'https://www.runoob.com', '小白学编程', 0, 'online', 0, '2021-12-18 15:42:56', '2021-12-18 15:42:52'),
(68, 11, '知乎', '', 'https://www.zhihu.com/', '知乎', 0, 'online', 0, '2021-12-18 15:46:46', '2021-12-18 15:46:41'),
(69, 11, '贴吧', '', 'https://tieba.baidu.com/', '', 0, 'online', 0, '2021-12-18 15:51:18', '2021-12-18 15:50:58'),
(70, 10, 'GitHub', '', 'https://github.com/', '全球最大技术知识开源平台', 0, 'online', 0, '2021-12-18 15:54:02', '2021-12-18 15:53:20'),
(71, 11, 'V2EX', '', 'https://www.v2ex.com/', '', 0, 'online', 0, '2021-12-18 15:54:29', '2021-12-18 15:54:24'),
(72, 12, '哔哩哔哩', '', 'https://www.bilibili.com', '', 0, 'online', 0, '2021-12-18 15:59:26', '2021-12-18 15:59:21'),
(73, 13, '有趣网址', '', 'https://youquhome.com', '', 0, 'online', 0, '2021-12-18 16:01:59', '2021-12-18 16:01:55'),
(74, 13, '少数派', '', 'https://sspai.com/', '', 0, 'online', 0, '2021-12-18 16:02:41', '2021-12-18 16:02:37'),
(75, 13, '煎蛋', '', 'https://jandan.net', '地球上没有新鲜事', 0, 'online', 0, '2021-12-19 05:15:00', '2021-12-18 16:03:29'),
(76, 13, '思谋学术', '', 'https://ac.scmor.com', 'Google学术发现世界', 0, 'online', 0, '2021-12-18 16:05:10', '2021-12-18 16:04:58'),
(77, 14, '在线修图', '', 'https://ps.gaoding.com/', '搞定修图-在线PS', 0, 'online', 0, '2021-12-18 16:08:22', '2021-12-18 16:07:33'),
(78, 14, '二维码', '', 'https://cli.im/', '草料二维码', 8, 'online', 0, '2021-12-19 06:31:18', '2021-12-18 16:09:06'),
(79, 11, '吾爱破解', '', 'https://www.52pojie.cn/', '', 0, 'online', 0, '2021-12-18 16:17:09', '2021-12-18 16:17:02'),
(80, 15, '茶杯狐', '', 'https://cupfox.app/', '影视搜索引擎', 0, 'online', 0, '2021-12-19 07:13:17', '2021-12-19 05:14:39'),
(81, 13, '有格调', '', 'https://www.ugediao.com', '', 0, 'online', 0, '2021-12-19 05:22:11', '2021-12-19 05:22:04'),
(82, 12, '泛贱志', '', 'https://www.ifanjian.net', '一个有内涵的网站', 0, 'online', 0, '2021-12-19 05:27:51', '2021-12-19 05:27:44'),
(83, 15, '鸠摩搜书', '', 'https://www.jiumodiary.com', '电子书搜索引擎', 0, 'online', 0, '2021-12-19 05:29:55', '2021-12-19 05:29:49'),
(84, 14, '公益节点', '', 'https://lncn.org', '', 0, 'online', 0, '2021-12-19 05:38:49', '2021-12-19 05:38:44'),
(85, 12, '影视精选', '', 'https://www.xiaochou.ren/archives/1', '', 0, 'online', 0, '2021-12-19 05:39:51', '2021-12-19 05:39:47'),
(86, 14, '图片压缩', '', 'https://xiaochou.ren/tools/tinyimg/index.html', '优化图片大小', 9, 'online', 0, '2021-12-19 06:31:05', '2021-12-19 05:45:56'),
(87, 12, '热歌榜', '', 'http://lite.tonzhon.com', '铜钟音乐', 0, 'online', 0, '2021-12-19 06:07:55', '2021-12-19 06:07:49'),
(88, 12, '随机小姐姐', '', 'https://xiaochou.ren/tools/douMM', '抖音快手小姐姐', 0, 'online', 0, '2021-12-19 07:11:56', '2021-12-19 07:11:39'),
(89, 14, 'Md5解密', '', 'http://cn.freemd5.com', '免费md5解密', 0, 'online', 0, '2021-12-19 07:56:54', '2021-12-19 07:56:50'),
(90, 14, '万能格式转换', '', 'https://cn.office-converter.com', 'PDF、MP3、JPG、电子书格式转换', 0, 'online', 0, '2021-12-19 08:01:32', '2021-12-19 08:01:26'),
(91, 15, '磁力猫', '', 'https://xn--tfrs17es0d.com/', '最好用的磁力链接搜索引擎，BT、种子搜索', 0, 'online', 0, '2021-12-19 08:21:28', '2021-12-19 08:21:21'),
(92, 15, '大力网盘', '', 'https://www.dalipan.com/', '网盘搜索引擎……', 0, 'online', 0, '2021-12-19 08:27:12', '2021-12-19 08:27:04'),
(93, 16, '小丑人分享', '', 'https://xiaochou.ren', '小丑人资源分享网', 0, 'online', 0, '2021-12-19 08:28:04', '2021-12-19 08:27:56'),
(94, 16, '域名抢注', '', 'http://90yuming.com/', '聚名网合作伙伴', 0, 'online', 0, '2021-12-19 08:29:25', '2021-12-19 08:29:17');

-- --------------------------------------------------------

--
-- 表的结构 `izy_notice_table`
--

CREATE TABLE `izy_notice_table` (
  `notice_id` int UNSIGNED NOT NULL,
  `notice` varchar(100) DEFAULT '这是一个公告！' COMMENT '公告',
  `notice_color` char(6) DEFAULT '000000',
  `link` varchar(500) DEFAULT '#' COMMENT '外链',
  `status` enum('delete','online','offline') DEFAULT 'online',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `izy_refer_info`
--

CREATE TABLE `izy_refer_info` (
  `refer_id` int NOT NULL,
  `url` varchar(500) NOT NULL DEFAULT '',
  `md5` char(32) NOT NULL DEFAULT '',
  `title` varchar(20) NOT NULL DEFAULT '',
  `keywords` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  `visit_total_num` int NOT NULL DEFAULT '0',
  `visit_today_num` int NOT NULL DEFAULT '0',
  `last_visit_time` datetime NOT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自动来源信息';

-- --------------------------------------------------------

--
-- 表的结构 `izy_sys_info`
--

CREATE TABLE `izy_sys_info` (
  `sys_id` int UNSIGNED NOT NULL,
  `logo_link` varchar(100) DEFAULT '/',
  `site_name` varchar(50) DEFAULT '爱资源导航站 - 有你需要的资源！',
  `site_keyword` varchar(50) DEFAULT '最新电影资源|在线工具',
  `site_desc` varchar(100) DEFAULT '一个日常必备寻找资源的导航站',
  `notice_limit` tinyint UNSIGNED NOT NULL DEFAULT '3',
  `link_show_limit` tinyint UNSIGNED NOT NULL DEFAULT '20' COMMENT '可展示链接数量',
  `per_page` tinyint UNSIGNED DEFAULT '20' COMMENT '所有分页',
  `footer_info` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '备案信息',
  `copy_right` varchar(50) DEFAULT NULL COMMENT '版权',
  `add_source` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否开启跳转链接添加本站来源参数',
  `cache_time` int NOT NULL DEFAULT '0' COMMENT '全站缓存时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `izy_sys_info`
--

INSERT INTO `izy_sys_info` (`sys_id`, `logo_link`, `site_name`, `site_keyword`, `site_desc`, `notice_limit`, `link_show_limit`, `per_page`, `footer_info`, `copy_right`, `add_source`, `cache_time`) VALUES
(1, '/', '爱资源导航站 - 你想要的这儿都有', '生活娱乐,在线工具,资源导航', '爱资源导航是一个专注于收集分享电影、小说阅读、音乐下载、在线工具等免费实用资源的导航站。', 3, 20, 20, '<a href=\"https://beian.miit.gov.cn/#/Integrated/recordQuery\" target=\"_blank\">冀ICP备19025082号</a>', '爱资源导航', 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `izy_task_info`
--

CREATE TABLE `izy_task_info` (
  `id` int UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `izy_user_info`
--

CREATE TABLE `izy_user_info` (
  `user_id` int UNSIGNED NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  `type` enum('admin','normal','vip') DEFAULT 'normal',
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `izy_user_info`
--

INSERT INTO `izy_user_info` (`user_id`, `username`, `password`, `type`, `state`, `create_time`) VALUES
(1, 'izy123', 'e10adc3949ba59abbe56e057f20f883e', 'normal', 1, '2021-12-18 14:28:19');

--
-- 转储表的索引
--

--
-- 表的索引 `izy_cate_info`
--
ALTER TABLE `izy_cate_info`
  ADD PRIMARY KEY (`cate_id`);

--
-- 表的索引 `izy_link_info`
--
ALTER TABLE `izy_link_info`
  ADD PRIMARY KEY (`link_id`);

--
-- 表的索引 `izy_notice_table`
--
ALTER TABLE `izy_notice_table`
  ADD PRIMARY KEY (`notice_id`);

--
-- 表的索引 `izy_refer_info`
--
ALTER TABLE `izy_refer_info`
  ADD PRIMARY KEY (`refer_id`),
  ADD UNIQUE KEY `refer_md5` (`md5`);

--
-- 表的索引 `izy_sys_info`
--
ALTER TABLE `izy_sys_info`
  ADD PRIMARY KEY (`sys_id`);

--
-- 表的索引 `izy_task_info`
--
ALTER TABLE `izy_task_info`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `izy_user_info`
--
ALTER TABLE `izy_user_info`
  ADD PRIMARY KEY (`user_id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `izy_cate_info`
--
ALTER TABLE `izy_cate_info`
  MODIFY `cate_id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- 使用表AUTO_INCREMENT `izy_link_info`
--
ALTER TABLE `izy_link_info`
  MODIFY `link_id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- 使用表AUTO_INCREMENT `izy_notice_table`
--
ALTER TABLE `izy_notice_table`
  MODIFY `notice_id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `izy_refer_info`
--
ALTER TABLE `izy_refer_info`
  MODIFY `refer_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- 使用表AUTO_INCREMENT `izy_sys_info`
--
ALTER TABLE `izy_sys_info`
  MODIFY `sys_id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `izy_task_info`
--
ALTER TABLE `izy_task_info`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `izy_user_info`
--
ALTER TABLE `izy_user_info`
  MODIFY `user_id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
