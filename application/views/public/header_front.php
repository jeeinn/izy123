<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-US">

<head>
    <?php
    //seo优化
    switch (true){
        case preg_match('/cate\/[0-9]+/',$_SERVER['REQUEST_URI'])==true:
            $seo_title = $cate_info['cate_name'].' - '.$cate_info['seo_title'];
            $seo_keywords = $cate_info['seo_keyword'];
            $seo_description = $cate_info['seo_desc'];
            break;
        case preg_match('/refer/',$_SERVER['REQUEST_URI'])==true:
            $seo_title = '自动收录网站链接 - 爱资源导航';
            $seo_keywords = '免费网站外链,网站SEO优化,免登陆发布链接,自助收录';
            $seo_description = '可以自动化收录网站链接，提高网站收录、曝光率、免费外链优化';
            break;
        default:
            $seo_title = $sys_info['site_name'];
            $seo_keywords = $sys_info['site_keyword'];
            $seo_description = $sys_info['site_desc'];
    }
    ?>
    <title><?php echo $seo_title;?></title>
    <meta name="keywords" content="<?php echo $seo_keywords;?>">
    <meta name="description" content="<?php echo $seo_description;?>">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
	<meta content="telephone=no,email=no" name="format-detection">
	<meta content="yes" name="apple-mobile-web-app-capable">
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="apple-touch-fullscreen" content="yes" />
    <link rel="shortcut icon" href="<?php echo base_url('statics/favicon.ico');?>">
	<!-- 注意！为了优化演示站访问速度下方两行引用的是CDN上的CSS文件地址，请修改为自己的样式文件地址 -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('statics/css/front_style.css?v1');?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('statics/css/front_mobile.css');?>" />
	<!-- 本项目开源地址：https://github.com/appexplore/jianavi 请勿完全抄袭源码，请勿声明自己是原创 -->
</head>
