<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

            <!-- 底部版权start -->
			<!-- 开源不易，请保留下方本项目开源地址 -->
			<!-- 如果您不愿意保留下方本项目开源地址，请打赏并留言您的网址 >> https://afdian.net/@appts -->

			<div class="footer">Copyright © <?php echo date('Y', time());?> <?php echo $sys_info['copy_right']; ?>
				<!-- 请保留下方开源地址-->
				<a href="https://github.com/appexplore/jianavi" style="color:#ffffff;" target="_blank" rel="nofollow">&nbsp;前端开源</a>
                <br>
                <span><?php echo $sys_info['footer_info']; ?></span>
			</div>
			<!-- 底部版权end -->
	</div>

	<!-- 注意！为了优化演示站访问速度下方两行引用的是CDN上的js文件地址，请修改为自己的样式文件地址 -->
    <script type="text/javascript" src="<?php echo base_url('statics/js/jquery.min.1.7.1.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('statics/js/front_js.js?v4');?>"></script>
    <script>
        <?php if ($sys_info['add_source']){?>
        $(function() {
            var self_url = window.location.hostname;
            document.addEventListener('click',function (e) {
                if(e.target.nodeName.toUpperCase()==='A'){
                    e.preventDefault();
                    var target_url = e.target.href;
                    if(target_url.indexOf(self_url) > -1){
                        window.open(target_url,'_self');
                        return false;
                    }
                    if(/[?&]/.test(target_url)){
                        target_url += '&';
                    }else {
                        target_url += '?';
                    }
                    target_url += 'utm_source='+self_url;
                    window.open(target_url);
                }
            },false);
        });
        <?php }?>
    </script>
</body>

</html>