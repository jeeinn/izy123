<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<body>

<!-- 搜索栏start -->
<div class="baidu baidu-2">
    <form name="f" action="https://www.baidu.com/s" target="_blank">
        <div class="Select-box-2" id="baidu">
            <ul>
                <li class="this_s">百 度</li>
                <li class="f_s">F 搜</li>
                <li class="wuzhui_s">无 追</li>
                <li class="bing_s">必 应</li>
                <li class="google_s">谷 歌</li>
                <li class="baidu_s">百 度</li>
            </ul>
        </div>
        <div class="in5">
            <input name="wd" id="kw-2" maxlength="100" autocomplete="off" type="text">
            <div class="clear" id="clear" title="清空">x</div>
        </div>
        <input type="hidden" name="utm_source" value="<?php echo $_SERVER['HTTP_HOST']?>">
        <input value="搜 索" id="su-2" type="submit">
    </form>
    <ul class="keylist"></ul>
</div>
<!-- 搜索栏end -->

<!-- 内容start -->
<div class="body">
    <div class="content">
        <?php foreach ($cates as $cate){?>
            <div class="jianjie">
                <div class="jj-list">
                    <div class="jj-list-tit" style="color:<?php echo '#' . $cate['cate_color'];?>;"><?php echo $cate['cate_name'];?></div>
                    <ul class="jj-list-con">
                        <?php foreach ($links as $link){
                            if($link['cate_id']==$cate['cate_id']){?>
                                <li><a href="<?php echo $link['link'];?>" title="<?php echo $link['intro']?>" class="link-3" target="_blank" rel="nofollow" style="color:<?php echo '#' . $link['title_color'];?>;"><?php echo $link['title'];?></a></li>
                            <?php }?>
                        <?php }?>
                    </ul>
                </div>
            </div>
        <?php }?>
    </div>
        <!-- 内容end -->